## A map of Singapore split by the nearest McDonald's.
Made using turf.js.

Inspired by [Joseph Tang's Singapre MRT Map](https://github.com/jtlx/singapore-mrt-voronoi)

### Attribution
- [McDonald's Singapore Store Outlets](https://www.mcdonalds.com.sg/locate-us/) accessed on 11 May 2020.
