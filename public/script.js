
mapboxgl.accessToken =
  "pk.eyJ1Ijoiam9yZGFubHlzOTUiLCJhIjoiY2thMjRoZjk5MDhkejNmbzV2NnQ3OGR2aCJ9.lKziTfam_bfJEQ43Eh3rig";

var options = {
  bbox: [103.580737, 1.13291, 104.093415, 1.473158]
};

var mcd_stores = turf.featureCollection([]);

const geolocate = new mapboxgl.GeolocateControl({
  positionOptions: {
    enableHighAccuracy: true
  },
  trackUserLocation: true
});

var map = initMap();

map.on("style.load", function () {


  map.addSource("mcd-voronoi", { type: "geojson", data: mcd_stores });
  map.addSource("mcd-stores", { type: "geojson", data: mcd_stores });
  map.addSource('nearest-mcd-line', { type: 'geojson', data: turf.featureCollection([]) });
  map.addSource('nearest-mcd-label', {
    'type': 'geojson',
    'data': turf.featureCollection([])
  });

  fetchStores();

  map.addControl(geolocate, 'bottom-left');
  map.setLayoutProperty('poi-label', 'visibility', 'none'); //remove random shop names

  map.addLayer({
    id: "mcd-stores",
    type: "circle",
    source: "mcd-stores",
    paint: {
      "circle-color": "#FFC72C",
      "circle-radius": 5,
      "circle-stroke-color": "#DA291c",
      "circle-stroke-opacity": 1,
      "circle-stroke-width": 2
    }
  });

  map.addLayer({
    id: "voronoi",
    type: "line",
    source: "mcd-voronoi",
    paint: {
      "line-width": 2,
      "line-color": "#FFC72C"
    }
  });

  map.addLayer({
    'id': 'nearest-mcd-line',
    'type': 'line',
    'source': 'nearest-mcd-line',
    'layout': {
      'line-join': 'round',
      'line-cap': 'round'
    },
    'paint': {
      'line-color': '#ff6ec7',
      'line-width': 1
    }
  })

  map.addLayer({
    'id': 'nearest-mcd-label',
    'type': 'symbol',
    'source': 'nearest-mcd-label',
    'layout': {
      'symbol-placement': 'line-center',
      // 'text-rotation-alignment': 'viewport',
      // 'text-keep-upright': true,
      'text-field': ['get', 'distance'],
    },
    'paint': {
      'text-translate': [0, 24],
      'text-color': '#ff6ec7'

    }
  });

})

map.on("load", function () {
  map.on('mouseenter', 'mcd-stores', function () {
    map.getCanvas().style.cursor = 'pointer';
  });

  // Change it back to a pointer when it leaves.
  map.on('mouseleave', 'mcd-stores', function () {
    map.getCanvas().style.cursor = '';
  });

  map.on("click", "mcd-stores", function (e) {
    new mapboxgl.Popup()
      .setLngLat(e.lngLat)
      .setHTML(`
      <strong>${e.features[0].properties.name}</strong>
      <p>
      ${e.features[0].properties.address}
      </p>

      `)
      .addTo(map);
  });

});

geolocate.on('geolocate', function (e) {
  var geoArray = [];
  var lon = e.coords.longitude;
  var lat = e.coords.latitude
  var position = [lon, lat];
  var userPoint = turf.point([lon, lat], { type: 'user' })
  geoArray.push(position);

  var nearest = turf.nearestPoint(userPoint, mcd_stores);
  console.log(position, nearest.properties.name);
  geoArray.push(turf.getCoords(nearest));
  var distance = turf.distance(userPoint, nearest, { units: 'kilometers' });
  var line = turf.lineString(geoArray, { name: 'nearest-mcd', distance: distance.toFixed(2) + 'KM' });
  map.getSource('nearest-mcd-line').setData(turf.featureCollection([line]));
  map.getSource('nearest-mcd-label').setData(turf.featureCollection([line]));

});

function initMap() {
  var _map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/light-v10",
    center: [103.8163, 1.3426],
    zoom: 10,
    hash: true
  });


  return _map;
}


function fetchStores() {
  fetch('mcd_store_locations.geojson')
    .then(response => response.json())
    .then(mcd_store_collection => {
      mcd_stores = mcd_store_collection;
      var voronoiPolygons = turf.voronoi(mcd_store_collection, options);
      map.getSource("mcd-voronoi").setData(voronoiPolygons)
      map.getSource("mcd-stores").setData(mcd_store_collection)

    })
}


